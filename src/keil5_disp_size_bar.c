/**
 * @file 文件名： keil5_disp_size_barv0.5.c
 * @author 作者： ageek_nikola  邮箱：(2503865771@qq.com)
 * @brief  功能介绍：
 * @version 版本：1.0
 * @date 日期：2023-07-28
 *
 * gitee开源地址:https://gitee.com/nikolan/keil5_disp_size_bar
 * 程序并非是以工程生成的二进制固件来分析出信息
 * 而是读取keil提供的工程文件，map文件，htm文件等文本文件把有用的信息整合以直观形式重新呈现。
 * 尽管我们通过分析map文件即可了解到大部分工程的ram和flash使用情况
 * 但是map文件输出的信息可读性太差了并不能很好的分悉出一些有用的片段
 * 我们完全可以写一些程序脚本去解析这些文本文件来获取到关键信息
 * 再重新以我们需要的更直观的形式展现出来
 * 我这里直接以我较为熟悉的C语言写了这个程序这个程序很小，能很好的直接集成在工程内做工具使用
 * 程序原理:读取工程文件和map文件来统计输出文本进度条数据,读取map各文件占比信息插入链表排序后导出表格。
 * 
 */

#include "keil5_disp_size_bar.h"

char *ram_keyword[] = {"ER$$", "RW_", "ER_RW", "ER_ZI"};  // 执行段按这个判断是不是ram，后面读取地址还会重新判断是不是irom
char *flash_keyword[] = {"flash", "rom", "ROM", "FLASH"}; // 执行段按这个判断是不是flash，后面读取地址还会重新判断是不是iram

// 以下全局变量都可以通过main函数传参改变
bool is_need_printf_msg = false; // 要不要输出运行信息
char bar_used_ch[5] = {"■"};     // 输出的进度条已经使用的字符
char bar_unused_ch[5] = {"_"};   // 输出的进度条还没使用的字符

char project_search_path[256] = {'\0'};
char map_search_path[256] = {'\0'};
char project_file_path[256] = {'\0'};
char map_file_path[256] = {'\0'};

project_info_t pinfo;
storage_t ram[30];
storage_t flash[30];
uint16_t storage_count = 0;
FILE *log_file = NULL;
char log_path[256] = {'\0'};

/**
 * @brief  功能介绍：主函数实现了整体逻辑的各个重要步骤
 * @param  参数：argc 传入的参数个数
 * @param  参数：argv 传入的字符串参数
 * @return 返回值： int
 */
int main(int argc, char *argv[])
{

    // main函数的参数输入，main函数的第一个参数argv[0]是系统自动传入，
    // 是程序文件的路径，其他参数则用户使用字符串传入
    switch (argc)
    {

    case 1:                                // 用户传入没有传入参数时
        strcpy(project_search_path, "./"); // 以当前exe的文件夹开始递归搜索
        strcpy(map_search_path, "./");     // 以当前exe的文件夹开始递归搜索
        break;

    case 6:                                   // 用户传入五个参数时
        strcpy(bar_unused_ch, argv[5]);       // 这个第四参数是进度条的代表未使用的字符,默认是'_'
    case 5:                                   // 用户传入四个参数时
        strcpy(bar_used_ch, argv[4]);         // 这个第四参数是进度条的代表已使用的字符,默认是'■'
    case 4:                                   // 用户传入三个参数时
        is_need_printf_msg = atoi(argv[3]);   // 这个第三个参数如果是1则开启消息输出，消息输出默认是0，没有输出的消息存在log文件上，出现错误时可以查看知道在哪出错
    case 3:                                   // 用户传入两个参数时
        strcpy(project_search_path, argv[1]); // 把参数作为工程文件查找递归遍历的路径
        strcpy(map_search_path, argv[2]);     // 把第二个参数作为map文件查找递归遍历的路径
        break;
    case 2:                                   // 用户传入一个参数时
        strcpy(project_search_path, argv[1]); // 把参数作为工程文件查找递归遍历的路径
        strcpy(map_search_path, argv[1]);     // 同时把参数作为map文件查找递归遍历的路径
        break;
    default:
        break;
    }

    pinfo.ram_conut = 0;          // 初始化ram数量为0
    pinfo.flash_count = 0;        // 初始化flash数量为0
    pinfo.xram.available_max = 0; // 初始化xram大小为0
    pinfo.iram.available_max = 0; // 初始化iram大小为0
    pinfo.irom.available_max = 0; // 初始化irom大小为0

if(!strstr(project_search_path,".uvproj"))
{
    if (!find_path(project_search_path, ".uvprojx", project_file_path))    // 查keil5工程路径
    {                                                                      // 找不到keil5工程就尝试找keil4工程
        if (!find_path(project_search_path, ".uvproj", project_file_path)) // 查keil4工程路径
        {
            print_erro_info("find_path:find project file fail"); // 递归目录也没找到工程文件
            return 1;
        }
    }
}else
{
strcpy(project_file_path,project_search_path);
}


    if (!parse_keil_porject(project_file_path)) // 解析工程信息
        print_erro_info("parse_keil_porject fail");

    // 以工程文件的target那name作为log的名称加上后缀_alog.txt

    strcat(strcat(strcpy(log_path, "./"), pinfo.target_name), "_alog.txt");

    log_file = fopen(log_path, "w+"); // 清空已存在log文件，创建并覆盖写入
    if (!log_file)
    {
        print_erro_info("log_file:file open w fail");
        return false;
    }

    print_msg("project_search_path", "%s", project_search_path); // 找到的工程递归搜索路径
    print_msg("map_search_path", "%s", map_search_path);         // 找到的map递归搜索路径
    print_msg("project_file_path", "%s", project_file_path);     // 找到的工程文件路径

    if (pinfo.core == _8051)
    {
               if(!strstr(project_search_path,".m51"))
         {
        if (find_path(map_search_path, ".m51", map_file_path))
        {
            print_msg("m51_file_path", "%s", map_file_path); // 找到的m51文件路径
          
        }
        else
        {
            print_erro_info("find m51 fail");
        }
         }else
         {
            strcpy(map_file_path,map_search_path);
         }
           print_msg("parse_keil_m51 statr", "");           // 解析m51
            if (!parse_keil_m51(map_file_path))              // 开始解析m51信息
                print_erro_info("parse_keil_m51 fail");
            print_msg("parse_keil_m51 over printf bar", ""); // 解析m51完成输出进度条
            if (pinfo.xram.available_max != 0)               // 不一定有xram，如果有才输出
                prtint_percentage_bar_8051(pinfo.xram);      // 输出xram的占用
            prtint_percentage_bar_8051(pinfo.iram);          // 输出iram的占用
            prtint_percentage_bar_8051(pinfo.irom);          // 输出irom的占用
    }
    else
    {
        if(!strstr(project_search_path,".map"))
         {
       if (find_path(map_search_path, ".map", map_file_path))
        {

            print_msg(" map_file_path", "%s", map_file_path); // 找到的map文件路径
            
        }
        else
        {

            print_erro_info("find map fail");
        }
         }else
         {
            strcpy(map_file_path,map_search_path);
         }
 
            print_msg("parse_keil_map start", "");            // 开始解析map
            if (!parse_keil_map(map_file_path))               // 解析map文件
                print_erro_info("parse_keil_map fail");
            print_msg("parse_keil_map over printf bar", ""); // 解析map完成输出进度条

            printf("ram:\n");
            for (storage_count = 0; storage_count < pinfo.ram_conut; storage_count++)
            {
                prtint_percentage_bar(ram[storage_count]);
            }

            printf("flash:\n");
            for (storage_count = 0; storage_count < pinfo.flash_count; storage_count++)
            {
                prtint_percentage_bar(flash[storage_count]);
            }
            parse_file_proportion_and_export_csv_xlsx(map_file_path);
    }

    fclose(log_file); // 关闭log文件

    return 0;
}

/**
 * @brief  功能介绍：输出错误信息, perror输出后会退出程序，如果不想退出则换为普通的printf
 * @param  参数：msg 自定义错误信息
 */
void print_erro_info(const char *msg)
{
    print_msg("erro","%s",msg);
    perror(msg);
}

/**
 * @brief  功能介绍：在当前目录下递归查找带关键词的文件返回其路径
 * @param  参数：dir_path 目录路径
 * @param  参数：file_key_word 文件关键词
 * @param  参数：file_path 找到的文件路径
 * @return 返回值： true 找到文件了
 * @return 返回值： false 没找到文件
 */
bool find_path(const char *dir_path, const char *file_key_word, char *file_path)
{
    char tmp_name[1024] = {0};
    DIR *dir = opendir(dir_path);
    struct dirent *st;
    struct stat sta;

    if (dir == NULL)
    {
        perror("find_path");
        return false;
    }

    while ((st = readdir(dir)) != NULL)
    {
        strcpy(tmp_name, dir_path);
        strcat(tmp_name, "/");
        strcat(tmp_name, st->d_name); // 新文件路径名

        if (stat(tmp_name, &sta) < 0)
        {
            perror("stat");
            continue;
        }

        if (S_ISDIR(sta.st_mode))
        { // 如果为目录文件
            if (strcmp("..", st->d_name) == 0 || strcmp(".", st->d_name) == 0)
            {
                continue; // 忽略当前目录和上一层目录
            }
            else
            {
                if (find_path(tmp_name, file_key_word, file_path)) // 递归读取
                {
                    closedir(dir);
                    return true;
                }
            }
        }
        else
        { // 不为目录则打印文件路径名
            if (strstr(tmp_name, file_key_word))
            {
                strcpy(file_path, tmp_name);
                closedir(dir);
                return true;
            }
        }
    }

    closedir(dir);
    return false;
}

/**
 * @brief  功能介绍：在文件搜索字符串返回开始位置
 * @param  参数：fp 文件指针
 * @param  参数：sub_str 需要匹配的子字符串
 * @param  参数：seek 初始位置
 * @param  参数：offset 偏移位置
 * @return 返回值： int64_t pos不为-1时就是找到了子字符串在文件的位置
 */
int64_t sreach_string(FILE *fp, char *sub_str, int seek, uint32_t offset)
{
    char str[1000];
    int64_t pos = 0;
    fseek(fp, offset, seek);

    while (NULL != fgets(str, 1000, fp))
    {

        if (strstr(str, sub_str))
        {
            //    printf("str:%s",str);
            pos = ftell(fp) - strlen(str);
            fseek(fp, pos, SEEK_SET);
            return pos;
        }
    }
    return EOF;
}

/**
 * @brief  功能介绍：解析工程文件，读取ToolsetName获取是不是8051，读取Device是什么mcu，读Cpu获取默认iram，xram和irom.
 * @param  参数：path 工程文件路径
 * @return 返回值： true 解析成功
 * @return 返回值： false 解析失败
 */
bool parse_keil_porject(char *path)
{
    FILE *fp;
    int64_t pos = 0;
    char str[1000];
    char *ch_p = NULL;
    fp = fopen(path, "r");

#if USED_XLSXWRITER_LIB
// printf("USED_XLSXWRITER_LIB\n");
#endif

    if (!fp)
    {
        print_erro_info("fopen:");
        return false;
    }

    while (pos != EOF)
    {
        pos = sreach_string(fp, "<TargetName>", SEEK_SET, pos);
        if (EOF == pos)
        {
            print_erro_info("sreach_string:<TargetName> Not found");
            fclose(fp); // 关闭文件
            break;
        }
        else
        {

            fseek(fp, pos - 5, SEEK_SET);
            fgets(str, 1000, fp);
            // printf("%s\n", str);
            if (sscanf(str, "%*[^<]<TargetName>%30[^<]", pinfo.target_name) == 1)
                print_msg("TargetName", "%s", pinfo.target_name);

            break;
            break;
        }
    }

    if (pos == EOF)
        pos = 0;

    while (pos != EOF)
    {
        pos = sreach_string(fp, "<ToolsetName>", SEEK_SET, pos);
        if (EOF == pos)
        {
            print_erro_info("sreach_string:<ToolsetName> Not found");
            fclose(fp); // 关闭文件
            break;
        }
        else
        {

            fseek(fp, pos, SEEK_SET);
            fgets(str, 1000, fp);
            if (NULL != (ch_p = strstr(str, "MCS-51")))
            {

                pinfo.core = _8051;
            }
            else
            {

                pinfo.core = _arm;
            }
            break;
        }
    }

    if (pos == EOF)
        pos = 0;

    while (pos != EOF)
    {
        pos = sreach_string(fp, "<Device>", SEEK_SET, pos);
        if (EOF == pos)
        {
            print_erro_info("sreach_string:<ToolsetName> Not found");
            fclose(fp); // 关闭文件
            break;
        }
        else
        {
            fseek(fp, pos, SEEK_SET);
            fgets(str, 1000, fp);
            if (sscanf(str, "%*[^<]<Device>%30[^<]", pinfo.mcu_name) == 1)
                print_msg("Device", "%s", pinfo.mcu_name);

            break;
        }
    }

    if (pos == EOF)
        pos = 0;
    while (pos != EOF)
    {
        pos = sreach_string(fp, "<Cpu>", SEEK_SET, pos);
        if (EOF == pos)
        {
            print_erro_info("sreach_string:<Cpu> Not found");
            fclose(fp); // 关闭文件
            break;
        }
        else
        {
            fseek(fp, pos, SEEK_SET);
            fgets(str, 1000, fp);

            strcpy(pinfo.iram.name, "iram");
            strcpy(pinfo.xram.name, "xram");
            strcpy(pinfo.irom.name, "irom");

            if (pinfo.core == _8051)
            {
                strcpy(pinfo.core_name, "8051");
                print_msg("core", "%s", pinfo.core_name);
            }
            else
            {
                ch_p = strstr(str, "CPUTYPE");
                sscanf(ch_p, "CPUTYPE(%30[^)]", pinfo.core_name);
                print_msg("core", "%s", pinfo.core_name);
            }

            ch_p = strstr(str, "IRAM");
            if (ch_p && sscanf(ch_p, "IRAM(%llx%*[-,]%llx))", &pinfo.iram.base_addr, &pinfo.iram.available_max) == 2)
            {
                print_msg("iram", "%#llx-%#llx", pinfo.iram.base_addr, pinfo.iram.available_max);
            }

            ch_p = strstr(str, "XRAM");

            if (ch_p && sscanf(ch_p, "XRAM(%llx%*[-,]%llx))", &pinfo.xram.base_addr, &pinfo.xram.available_max) == 2)
            {
                print_msg("xram", "%#llx-%#llx", pinfo.xram.base_addr, pinfo.xram.available_max);
            }

            ch_p = strstr(str, "IROM");
            if (ch_p && sscanf(ch_p, "IROM(%llx%*[-,]%llx))", &pinfo.irom.base_addr, &pinfo.irom.available_max) == 2)
            {
                print_msg("irom", "%#llx-%#llx", pinfo.irom.base_addr, pinfo.irom.available_max);
            }

            break;
        }
    }
    fclose(fp); // 关闭文件
    return true;
}

/**
 * @brief  功能介绍：解析map文件,获取ram和flash信息
 * @param  参数：path map文件路径
 * @return 返回值： true 解析成功
 * @return 返回值： false 解析失败
 */
bool parse_keil_map(char *path)
{
    FILE *fp;
    int64_t pos = 0;
    char str[1000];
    char *ch_p = NULL;
    storage_t temp;
    uint16_t cnt;
    fp = fopen(path, "r");

    if (!fp)
    {
        print_erro_info("fopen:");
        return false;
    }

    while (pos != EOF)
    {
        pos = sreach_string(fp, "Execution Region", SEEK_SET, pos);
        if (EOF == pos)
        {
            // print_erro_info("sreach_string:Execution Region Not found");
            fclose(fp); // 关闭文件
            break;
        }
        else
        {
            print_msg("Found keyword", "----------%s----------", "Execution Region");
            fseek(fp, pos, SEEK_SET);
            fgets(str, 1000, fp);

            // printf("%s\n",str);

            if (sscanf(str, "%*[^E]Execution Region%50[^(]", temp.name) == 1)
            {
                print_msg("name", "%s", temp.name);
            }
            temp.type = _flash;
            for (cnt = 0; cnt < sizeof(ram_keyword) / sizeof(char *); cnt++)
            {
                if (strstr(temp.name, ram_keyword[cnt]))
                    break;
            }
            if (cnt != sizeof(ram_keyword) / sizeof(char *))
                temp.type = _ram;

            ch_p = strstr(str, "Base");
            if (ch_p && sscanf(ch_p, "Base:%*[^x]x%llx", &temp.base_addr) == 1)
            {
                print_msg("Base", "%#llx", temp.base_addr);
            }else {
                      ch_p = strstr(str, "Exec base:");
                if(ch_p && sscanf(ch_p, "Exec base:%*[^x]x%llx", &temp.base_addr) == 1)
                 {
                   print_msg("Exec base", "%#llx", temp.base_addr);
                 }
              }
            if (temp.base_addr >= pinfo.iram.base_addr && temp.base_addr <= pinfo.iram.base_addr + pinfo.iram.available_max)
                temp.type = _ram;
            else if (temp.base_addr >= pinfo.irom.base_addr && temp.base_addr <= pinfo.irom.base_addr + pinfo.irom.available_max)
                temp.type = _flash;

            ch_p = strstr(str, "Size");
            if (ch_p && sscanf(ch_p, "Size:%*[^x]x%llx", &temp.used_size) == 1)
            {
                print_msg("Size", "%#llx", temp.used_size);
            }

            ch_p = strstr(str, "Max");
            if (ch_p && sscanf(ch_p, "Max:%*[^x]x%llx", &temp.available_max) == 1)
            {
                print_msg("Max", "%#llx", temp.available_max);
            }

            if (temp.type == _ram)
            {
                ram[pinfo.ram_conut++] = temp;
            }
            else
            {
                flash[pinfo.flash_count++] = temp;
            }

            pos += strlen(str);
        }
    }
    // printf("close_map\n");
    fclose(fp); // 关闭文件
    return true;
}

/**
 * @brief  功能介绍：解析m51文件，获取iram和irom信息
 * @param  参数：path m51文件路径
 * @return 返回值： true 解析成功
 * @return 返回值： false 解析失败
 */
bool parse_keil_m51(char *path)
{
    FILE *fp;
    int64_t pos = 0;
    char str[1000];
    char *ch_p = NULL;

    float data, xdata, code;
    fp = fopen(path, "r");

    if (!fp)
    {
        print_erro_info("fopen:");
        return false;
    }

    while (pos != EOF)
    {
        pos = sreach_string(fp, "Program Size", SEEK_SET, pos);
        if (EOF == pos)
        {
            // print_erro_info("sreach_string:Program Size Not found");
            fclose(fp); // 关闭文件
            return false;
        }
        else
        {
            fseek(fp, pos - 1, SEEK_SET);
            fgets(str, 1000, fp);
            // printf("%s\n",str);

            ch_p = strstr(str, "xdata");
            if (ch_p && sscanf(ch_p, "xdata=%f", &xdata) == 1)
            {
                print_msg("xdata", "%f", xdata);
            }

            ch_p = strstr(str, "data");
            if (ch_p && sscanf(ch_p, "data=%f", &data) == 1)
            {
                print_msg("data", "%f", data);
            }

            ch_p = strstr(str, "code");
            if (ch_p && sscanf(ch_p, "code=%f", &code) == 1)
            {
                print_msg("code", "%f", code);
            }

            pinfo.iram.used_size = data * 10;
            pinfo.xram.used_size = xdata * 10;
            pinfo.irom.used_size = code * 10;
            break;
        }
    }
    fclose(fp); // 关闭文件
    return true;
}

/**
 * @brief  功能介绍：打印程序运行信息，默认关闭，输出到 工程target_alog.txt文件，is_need_printf_msg-true开启则同时输出到终端
 * @param  参数：tag 标签
 * @param  参数：format 格式符
 * @param  参数：... 输出数据参数
 */
void print_msg(const char *tag, const char *format, ...)
{
    if (is_need_printf_msg)
    {
        printf("[%s]: ", tag);
        va_list args;
        va_start(args, format);
        vprintf(format, args);
        va_end(args);
        printf("\n");
    }

    if (log_file != NULL)
    {

        // 输出到日志文件
        fprintf(log_file, "[%s]: ", tag);

        va_list args;
        va_start(args, format);
        vfprintf(log_file, format, args);
        va_end(args);

        fprintf(log_file, "\n");
        fflush(log_file); // 刷新文件流，确保内容写入文件
    }
}

/**
 * @brief  功能介绍：打印进度条占比信息
 * @param  参数：storage 存储，可以是ram或者flash，使用ram和flash数组内的有效信息来输出
 */
void prtint_percentage_bar(storage_t storage)
{
    if(storage.available_max==0)
    {
    print_msg("prtint_percentage_bar:","fail Unable to output percentage, denominator storage.available_max=0");
    print_erro_info("fail Unable to output percentage, denominator storage.available_max=0");
    
    }else if(storage.available_max>64*1024*1024)//早期stm32f0系列max是一个很大的值不是实际可以用的max
    {
        //以工程默认定义里的max取代map内错误的max
       if(storage.type==_ram)
       storage.available_max=pinfo.iram.available_max;
       else if(storage.type==_flash)
       {
        storage.available_max=pinfo.irom.available_max;
       }
    }

    uint8_t i;
    printf(" %s:%#llx\n", storage.name, storage.base_addr);
    if (log_file)
        fprintf(log_file, " %s:%#llx\n", storage.name, storage.base_addr);
    if (storage.available_max < 1024.0f * 1024.0f)
    {
        printf("%8.2f KB :|", (storage.available_max / 1024.0f));
        if (log_file)
            fprintf(log_file, "%8.2f KB :|", (storage.available_max / 1024.0f));
    }
    else
    {
        printf("%8.2f MB :|", (storage.available_max / 1024.0f / 1024.0f));
        if (log_file)
            fprintf(log_file, "%8.2f MB :|", (storage.available_max / 1024.0f / 1024.0f));
    }

    for (i = 0; i < 20; i++)
    {
        if (i < (uint8_t)((storage.used_size / 1.0) / (storage.available_max / 1.0) * 20))
        {

            printf("%s", bar_used_ch);
            if (log_file)
                fprintf(log_file, "%s", bar_used_ch);
        }
        else
        {
            printf("%s", bar_unused_ch);
            if (log_file)
                fprintf(log_file, "%s", bar_unused_ch);
        }
    }

    if (storage.available_max < 1024.0f * 1024.0f)
    {
        printf("|%8.2f %% (%8.2f KB / %8.2f KB)", (storage.used_size / 1.0) / (storage.available_max / 1.0) * 100, storage.used_size / 1024.0f, storage.available_max / 1024.0f);
        if (log_file)
            fprintf(log_file, "|%8.2f %% (%8.2f KB / %8.2f KB)", (storage.used_size / 1.0) / (storage.available_max / 1.0) * 100, storage.used_size / 1024.0f, storage.available_max / 1024.0f);
    }
    else
    {
        printf("|%8.2f %% (%8.2f MB / %8.2f MB)", (storage.used_size / 1.0) / (storage.available_max / 1.0) * 100, storage.used_size / 1024.0f / 1024.0f, storage.available_max / 1024.0f / 1024.0f);
        if (log_file)
            fprintf(log_file, "|%8.2f %% (%8.2f MB / %8.2f MB)", (storage.used_size / 1.0) / (storage.available_max / 1.0) * 100, storage.used_size / 1024.0f / 1024.0f, storage.available_max / 1024.0f / 1024.0f);
    }
    printf(" [%llu B]\n", storage.available_max - storage.used_size);
    if (log_file)
        fprintf(log_file, " [%llu B]\n", storage.available_max - storage.used_size);
}

/**
 * @brief  功能介绍：
 * @param  参数：storage 存储，直接用工程信息pinfo的xram iram irom，因为一般8051只有xram iram irom等没有扩展的ram或者flash的设计
 */
void prtint_percentage_bar_8051(storage_t storage)
{
    uint8_t i;
    if(storage.available_max==0)
    {
    print_msg("prtint_percentage_bar:","fail Unable to output percentage, denominator storage.available_max=0");
    print_erro_info("fail Unable to output percentage, denominator storage.available_max=0");
    
    }
    printf(" %s:%#llx\n", storage.name, storage.base_addr);
    if (log_file)
        fprintf(log_file, " %s:%#llx\n", storage.name, storage.base_addr);
    if (storage.available_max < 1024.0f)
    {
        printf("%8.2f  B :|", (storage.available_max / 1.0f));
        if (log_file)
            fprintf(log_file, "%8.2f  B :|", (storage.available_max / 1.0f));
    }
    else
    {
        printf("%8.2f KB :|", (storage.available_max / 1024.0f));
        if (log_file)
            fprintf(log_file, "%8.2f KB :|", (storage.available_max / 1024.0f));
    }

    for (i = 0; i < 20; i++)
    {
        if (i < (uint8_t)((storage.used_size / 10.0) / (storage.available_max / 1.0) * 20))
        {

            printf("%s", bar_used_ch);
            if (log_file)
                fprintf(log_file, "%s", bar_used_ch);
        }
        else
        {
            printf("%s", bar_unused_ch);
            if (log_file)
                fprintf(log_file, "%s", bar_unused_ch);
        }
    }

    if (storage.available_max / 1024.0f)
    {
        printf("|%8.2f %% (%lld.%lld B / %8.2f B)", (storage.used_size / 10.0) / (storage.available_max / 1.0) * 100, (uint64_t)(storage.used_size / 10.0), storage.used_size % 10, storage.available_max / 1.0f);
        if (log_file)
            fprintf(log_file, "|%8.2f %% (%lld.%lld B / %8.2f B)", (storage.used_size / 10.0) / (storage.available_max / 1.0) * 100, (uint64_t)(storage.used_size / 10.0), storage.used_size % 10, storage.available_max / 1.0f);
    }
    else
    {
        printf("|%8.2f %% (%lld.%lld KB / %8.2f KB)", (storage.used_size / 10.0) / (storage.available_max / 1.0) * 100, (uint64_t)(storage.used_size / 10.0 / 1024.0f), (uint64_t)((uint64_t)(storage.used_size / 1024.f) % 10), storage.available_max / 1024.0f);
        if (log_file)
            fprintf(log_file, "|%8.2f %% (%lld.%lld KB / %8.2f KB)", (storage.used_size / 10.0) / (storage.available_max / 1.0) * 100, (uint64_t)(storage.used_size / 10.0 / 1024.0f), (uint64_t)((uint64_t)(storage.used_size / 1024.f) % 10), storage.available_max / 1024.0f);
    }
    printf(" [%f B]\n", storage.available_max - storage.used_size / 10.0);
    if (log_file)
        fprintf(log_file, " [%f B]\n", storage.available_max - storage.used_size / 10.0);
}

/**
 * @brief  功能介绍：导出已使用的ram和flash里文件的占比信息
 * @param  参数：path map文件路径
 * @return 返回值： true 解析并导出成功
 * @return 返回值： false 解析和导出失败
 */
bool parse_file_proportion_and_export_csv_xlsx(char *path)
{
    FILE *fp;
    int64_t pos = 0;
    char str[1000];
    uint16_t cnt;
    fp = fopen(path, "r");
    map_info_item_t info_item;
    bool is_item_data = false;

    info_node_t *list_flash = NULL;
    info_node_t *list_ram = NULL;
    info_node_t *new_flash_insert_node = NULL;
    info_node_t *new_ram_insert_node = NULL;

    uint64_t ram_used = 0;
    uint64_t flash_used = 0;

    if (!fp)
    {
        print_erro_info("fopen:");
        return false;
    }

    while (pos != EOF)
    {
        pos = sreach_string(fp, "Image component sizes", SEEK_SET, pos);
        if (EOF == pos)
        {
            // print_erro_info("sreach_string:Execution Region Not found");
            fclose(fp); // 关闭文件
            break;
        }
        else
        {
            fseek(fp, pos - 1, SEEK_SET);
            fgets(str, 1000, fp);

            // printf("%s\n", str);

            pos += (strlen(str));
            break;
        }
    }

    if (EOF == pos)
        pos = 0;
    is_item_data = true;
    while (fgets(str, sizeof(str), fp))
    {

        cnt = sscanf(str, "%llu%llu%llu%llu%llu%llu%29s",
                     &info_item.Code, &info_item.inc_data, &info_item.RO_data,
                     &info_item.RW_data, &info_item.ZI_data, &info_item.Debug,
                     info_item.Object_name);

        if (cnt == 7)
        {
            if (strstr(info_item.Object_name, ".o") || strstr(info_item.Object_name, ".l") || strstr(info_item.Object_name, ".LIB"))
            {

                info_item.flash_size = info_item.Code + info_item.RO_data + info_item.RW_data;
                info_item.ram_size = info_item.RW_data + info_item.ZI_data;

                if (info_item.ram_size != 0)
                {
                    ram_used += info_item.ram_size;
                    new_ram_insert_node = create_node(info_item);
                    insert_in_sorted_order_by_ram_size(&list_ram, new_ram_insert_node);
                }
                if (info_item.flash_size != 0)
                {

                    flash_used += info_item.flash_size;
                    new_flash_insert_node = create_node(info_item);
                    insert_in_sorted_order_by_flash_size(&list_flash, new_flash_insert_node);
                }
            }
            else
            {

                if (strstr(str, "------"))
                {
                    if (is_item_data)
                        is_item_data = false;
                    else
                        is_item_data = true;
                }
                if (is_item_data)
                {
                    // printf("%s\n",str);
                }
            }
        }

        pos += strlen(str);
    }

    for (info_node_t *current = list_flash; current != NULL; current = current->next)
    {
        current->data.flash_percent = current->data.flash_size * 100.0 / 1.0 / flash_used;
    }

    for (info_node_t *current = list_ram; current != NULL; current = current->next)
    {
        current->data.ram_percent = current->data.ram_size / 1.0 * 100.0 / ram_used;
    }
    export_csv("./", list_ram, list_flash);

#if USED_XLSXWRITER_LIB
    export_xlsx("./", list_ram, list_flash);
#endif

    free_linked_list(&list_ram);
    free_linked_list(&list_flash);
    return true;
}
bool export_csv(char *path, info_node_t *list_ram, info_node_t *list_flash)
{

    FILE *csv_file_sort_by_flash = NULL;
    FILE *csv_file_sort_by_ram = NULL;
    char csv_path_sort_by_flash[256] = {'\0'};
    char csv_path_sort_by_ram[256] = {'\0'};
    info_node_t *current = NULL;
    strcat(strcat(strcpy(csv_path_sort_by_flash, path), pinfo.target_name), "_sort_by_flash.csv");
    csv_file_sort_by_flash = fopen(csv_path_sort_by_flash, "w"); // 清空已存在log文件，创建并覆盖写入
    if (!csv_file_sort_by_flash)
    {
        print_erro_info("csv_file_sort_by_flash:file open w fail If you use excel to open the csv table, please close the file first pieces");
        return false;
    }

    strcat(strcat(strcpy(csv_path_sort_by_ram, path), pinfo.target_name), "_sort_by_ram.csv");
    csv_file_sort_by_ram = fopen(csv_path_sort_by_ram, "w"); // 清空已存在log文件，创建并覆盖写入
    if (!csv_file_sort_by_ram)
    {
        print_erro_info("csv_file_sort_by_ram:file open w fail If you use excel to open the csv table, please close the file first pieces");
        return false;
    }

    fprintf(csv_file_sort_by_flash, "File_name,flash percent,flash,ram,Code,RO_data,RW_data,ZI_data\n");
    fprintf(csv_file_sort_by_ram, "File_name,ram_percent,ram,flash,Code,RO_data,RW_data,ZI_data\n");

    for (current = list_flash; current != NULL; current = current->next)
    {

        fprintf(csv_file_sort_by_flash, "%s,%f%%,%llu,%llu,%llu,%llu,%llu,%llu\n", current->data.Object_name, current->data.flash_percent, current->data.flash_size, current->data.ram_size, current->data.Code, current->data.RO_data, current->data.RW_data, current->data.ZI_data);
    }

    for (current = list_ram; current != NULL; current = current->next)
    {

        fprintf(csv_file_sort_by_ram, "%s,%f%%,%llu,%llu,%llu,%llu,%llu,%llu\n", current->data.Object_name, current->data.ram_percent, current->data.ram_size, current->data.flash_size, current->data.Code, current->data.RO_data, current->data.RW_data, current->data.ZI_data);
    }

    fclose(csv_file_sort_by_flash);
     print_msg("export csv", "%s",csv_path_sort_by_flash);
    fclose(csv_file_sort_by_ram);
 print_msg("export csv", "%s",csv_path_sort_by_ram);
    return true;
}

#if USED_XLSXWRITER_LIB

// 定义颜色数组，可以按你提供的颜色添加更多颜色
// uint32_t colors[] = {
//     0xFFD1FF4D, // Light Green Yellow
//     0xFFC0FF3E, // Greenish Yellow
//     0xFFADFF2F, // Green Yellow
//     0xFF7CFC00, // Lawn Green
//     0xFF00FF00, // Lime Green
//     0xFF008000, // Green
//     0xFF006400, // Dark Green
// };

bool export_xlsx(char *path, info_node_t *list_ram, info_node_t *list_flash)
{
    info_node_t *current = NULL;
    xlsx_data_t xlsx_data;
    uint64_t row = 0;
    lxw_chart_series *series;
    // lxw_chart_point *points = NULL;
    char categories[100] = {'\0'};
    char values[100] = {'\0'};
    char str_buf1[100] = {'\0'};
    char str_buf2[100] = {'\0'};
    char xlsx_path[256] = {'\0'};

    strcat(strcat(strcpy(xlsx_path, path), pinfo.target_name), "_analysis.xlsx");
    // 检查文件是否存在，如果存在就删除
    if (access(xlsx_path, F_OK) == 0)
    {
        if (remove(xlsx_path) != 0)
        {
            printf("Error deleting %s If you opened this file using Excel, please close it first\n", xlsx_path);
            return -1;
        }
    }

    xlsx_data.workbook = workbook_new(xlsx_path);
    xlsx_data.worksheet_ram_percent = workbook_add_worksheet(xlsx_data.workbook, "ram_percent");
    xlsx_data.worksheet_flash_percent = workbook_add_worksheet(xlsx_data.workbook, "flash_percent");

    /* Write the worksheet caption to explain the example. */
    worksheet_write_string(xlsx_data.worksheet_ram_percent, CELL("A1"), pinfo.target_name, NULL);
    worksheet_write_string(xlsx_data.worksheet_flash_percent, CELL("A1"), pinfo.target_name, NULL);

    lxw_format *currency_format = workbook_add_format(xlsx_data.workbook);
    format_set_align(currency_format, LXW_ALIGN_CENTER);
    lxw_format *float_format = workbook_add_format(xlsx_data.workbook);
    format_set_num_format(float_format, "00.00"); // 设置小数点后两位
    format_set_align(float_format, LXW_ALIGN_CENTER);

    /* Set the table options. */
    lxw_table_column col_1 = {.header = "File_name",
                              .total_string = "Totals"};

    lxw_table_column col_2 = {.header = "ram_percent",
                              .total_function = LXW_TABLE_FUNCTION_SUM};
    lxw_table_column col_2_1 = {.header = "flash_percent",
                                .total_function = LXW_TABLE_FUNCTION_SUM};
    lxw_table_column col_3 = {.header = "ram",
                              .total_function = LXW_TABLE_FUNCTION_SUM};

    lxw_table_column col_4 = {.header = "flash",
                              .total_function = LXW_TABLE_FUNCTION_SUM};

    lxw_table_column col_5 = {.header = "Code",
                              .total_function = LXW_TABLE_FUNCTION_SUM};

    lxw_table_column col_6 = {.header = "RO_data",
                              .total_function = LXW_TABLE_FUNCTION_SUM};

    lxw_table_column col_7 = {.header = "RW_data",
                              .total_function = LXW_TABLE_FUNCTION_SUM};
    lxw_table_column col_8 = {.header = "ZI_data",
                              .total_function = LXW_TABLE_FUNCTION_SUM};

    lxw_table_column *columns_ram[] = {&col_1, &col_2, &col_3, &col_4,
                                       &col_5, &col_6, &col_7, &col_8, NULL};

    lxw_table_column *columns_flash[] = {&col_1, &col_2_1, &col_4, &col_3,
                                         &col_5, &col_6, &col_7, &col_8, NULL};

    lxw_table_options options_ram = {
        .style_type = LXW_TABLE_STYLE_TYPE_LIGHT,
        .style_type_number = 11,
        .total_row = LXW_TRUE,
        .columns = columns_ram};

    lxw_table_options options_flash = {
        .style_type = LXW_TABLE_STYLE_TYPE_LIGHT,
        .style_type_number = 11,
        .total_row = LXW_TRUE,
        .columns = columns_flash};

    for (current = list_ram, row = 2; current != NULL; current = current->next, row++)
    {

        worksheet_write_string(xlsx_data.worksheet_ram_percent, row, 0, current->data.Object_name, currency_format);

        worksheet_write_number(xlsx_data.worksheet_ram_percent, row, 1, current->data.ram_percent, float_format);

        worksheet_write_number(xlsx_data.worksheet_ram_percent, row, 2, current->data.ram_size, currency_format);
        worksheet_write_number(xlsx_data.worksheet_ram_percent, row, 3, current->data.flash_size, currency_format);
        worksheet_write_number(xlsx_data.worksheet_ram_percent, row, 4, current->data.Code, currency_format);
        worksheet_write_number(xlsx_data.worksheet_ram_percent, row, 5, current->data.RO_data, currency_format);
        worksheet_write_number(xlsx_data.worksheet_ram_percent, row, 6, current->data.RW_data, currency_format);
        worksheet_write_number(xlsx_data.worksheet_ram_percent, row, 7, current->data.ZI_data, currency_format);
    }

    for (current = list_flash, row = 2; current != NULL; current = current->next, row++)
    {

        worksheet_write_string(xlsx_data.worksheet_flash_percent, row, 0, current->data.Object_name, currency_format);

        worksheet_write_number(xlsx_data.worksheet_flash_percent, row, 1, current->data.flash_percent, float_format);

        worksheet_write_number(xlsx_data.worksheet_flash_percent, row, 2, current->data.flash_size, currency_format);
        worksheet_write_number(xlsx_data.worksheet_flash_percent, row, 3, current->data.ram_size, currency_format);
        worksheet_write_number(xlsx_data.worksheet_flash_percent, row, 4, current->data.Code, currency_format);
        worksheet_write_number(xlsx_data.worksheet_flash_percent, row, 5, current->data.RO_data, currency_format);
        worksheet_write_number(xlsx_data.worksheet_flash_percent, row, 6, current->data.RW_data, currency_format);
        worksheet_write_number(xlsx_data.worksheet_flash_percent, row, 7, current->data.ZI_data, currency_format);
    }
    /* Add a table to the worksheet. */

    worksheet_add_table(xlsx_data.worksheet_ram_percent, 1, 0, row, 7, &options_ram);
    worksheet_add_table(xlsx_data.worksheet_flash_percent, 1, 0, row, 7, &options_flash);

    /* Set the columns widths for clarity. */

    worksheet_set_column(xlsx_data.worksheet_ram_percent, COLS("A:H"), 16, NULL);
    worksheet_set_column(xlsx_data.worksheet_flash_percent, COLS("A:H"), 16, NULL);

    /*
     * Chart 3: Create a pie chart with rotation of the segments.
     */

    xlsx_data.piechart_ram = workbook_add_chart(xlsx_data.workbook, LXW_CHART_PIE);
    xlsx_data.piechart_flash = workbook_add_chart(xlsx_data.workbook, LXW_CHART_PIE);

    /* Add some points with the above fills. */

    /* Add the first series to the chart. */

    lxw_rowcol_to_formula_abs(str_buf1, xlsx_data.worksheet_ram_percent->name, 2, 0, row, 0);
    sprintf(categories, "=%s", str_buf1);
    // printf("buf1: %s\n",categories);

    lxw_rowcol_to_formula_abs(str_buf2, xlsx_data.worksheet_ram_percent->name, 2, 1, row, 1);
    sprintf(values, "=%s", str_buf2);
    // printf("buf2: %s\n",values);

    series = chart_add_series(xlsx_data.piechart_ram, categories, values);

    /* Set the name for the series instead of the default "Series 1". */
    chart_series_set_name(series, "piechart_ram");

    /* Add a chart title. */
    chart_title_set_name(xlsx_data.piechart_ram, "file percent in used ram ");

    /* Change the angle/rotation of the first segment. */
    chart_set_rotation(xlsx_data.piechart_ram, 90);

    /* Insert the chart into the worksheet. */
    worksheet_insert_chart(xlsx_data.worksheet_ram_percent, CELL("I2"), xlsx_data.piechart_ram);

    lxw_rowcol_to_formula_abs(str_buf1, xlsx_data.worksheet_flash_percent->name, 2, 0, row, 0);
    sprintf(categories, "=%s", str_buf1);
    // printf("buf1: %s\n",categories);

    lxw_rowcol_to_formula_abs(str_buf2, xlsx_data.worksheet_flash_percent->name, 2, 1, row, 1);
    sprintf(values, "=%s", str_buf2);
    // printf("buf2: %s\n",values);

    series = chart_add_series(xlsx_data.piechart_flash, categories, values);

    /* Set the name for the series instead of the default "Series 1". */
    chart_series_set_name(series, "piechart_flash");

    /* Add a chart title. */
    chart_title_set_name(xlsx_data.piechart_flash, "file percent in used flash ");

    /* Change the angle/rotation of the first segment. */
    chart_set_rotation(xlsx_data.piechart_flash, 90);

    /* Insert the chart into the worksheet. */
    worksheet_insert_chart(xlsx_data.worksheet_flash_percent, CELL("I2"), xlsx_data.piechart_flash);

    // printf("save\n");
    workbook_close(xlsx_data.workbook);
 print_msg("export xlsx", "%s_analysis.xlsx",pinfo.target_name);
    return true;
}
#endif
 

/**
 * @brief  功能介绍：创建新节点
 * @param  参数：item info信息数据结构体
 * @return 返回值： info_node_t* 创建出来的带数据的新结点
 */
info_node_t *create_node(map_info_item_t item)
{
    info_node_t *new_node = (info_node_t *)malloc(sizeof(info_node_t));
    if (new_node == NULL)
    {
        perror("malloc:Memory allocation failed!\n");
        return NULL;
    }
    new_node->data = item;
    new_node->next = NULL;
    return new_node;
}


/**
 * @brief  功能介绍：按flash_size降序插入节点到已排序链表的合适位置
 * @param  参数：head list_flash的头结点地址
 * @param  参数：new_node  新结点
 */
void insert_in_sorted_order_by_flash_size(info_node_t **head, info_node_t *new_node)
{
    if (*head == NULL || (*head)->data.flash_size <= new_node->data.flash_size)
    {
        new_node->next = *head;
        *head = new_node;
    }
    else
    {
        info_node_t *current = *head;
        while (current->next != NULL && current->next->data.flash_size > new_node->data.flash_size)
        {
            current = current->next;
        }
        new_node->next = current->next;
        current->next = new_node;
    }
}


/**
 * @brief  功能介绍：按ram_size降序插入节点到已排序链表的合适位置
 * @param  参数：head list_ram的头结点地址
 * @param  参数：new_node 新结点
 */
void insert_in_sorted_order_by_ram_size(info_node_t **head, info_node_t *new_node)
{
    if (*head == NULL || (*head)->data.ram_size <= new_node->data.ram_size)
    {
        new_node->next = *head;
        *head = new_node;
    }
    else
    {
        info_node_t *current = *head;
        while (current->next != NULL && current->next->data.ram_size > new_node->data.ram_size)
        {
            current = current->next;
        }
        new_node->next = current->next;
        current->next = new_node;
    }
}


/**
 * @brief  功能介绍：释放链表内存
 * @param  参数：head 链表地址
 */
void free_linked_list(info_node_t **head)
{
    while ((*head)->next != NULL)
    {
        info_node_t *temp = *head;
        *head = (*head)->next;
        free(temp);
    }
}

